//
//  LicenseView.m
//  IdentificationMaster
//
//  Created by Рома on 3/24/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "LicenseView.h"
#import "LoginView.h"
#import "UserModel.h"

#import <AFNetworking/AFNetworking.h>

@interface LicenseView ()

@property (strong) IBOutlet NSTextField *iAdminPassword;
@property (strong) IBOutlet NSTextField *iAdminPasswordRepeate;


@end

@implementation LicenseView


- (void)checkInputLicenseOnServer:(NSString *)inpulLicense withBlock:(DictionaryResultBlock)block
{
    
    NSString *urlString = @"http://private-c4af32-tamplatesapi.apiary-mock.com/license";
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if (block) {
            block(responseObject, nil);
        }
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        if (block) {
            block(nil, error);
        }
    }];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}


- (IBAction)iRegisterSoftwareButtonPushed:(id)sender {
    /*@property (strong) IBOutlet NSTextField *iLicenseInputField;
     
     @property (strong) IBOutlet NSButton *iRegisterSoftwareButton;
     
     @property (strong) IBOutlet NSTextField *iErrorlLabel;
     
*/
    
    NSString *inputKey = [self.iLicenseInputField stringValue];
    [self checkInputLicenseOnServer:inputKey withBlock:^(NSDictionary *object, NSError *error) {
       

        if (object) {
            
            NSString *returnKey = [object objectForKey:@"license"];
            
            if ([returnKey isEqualToString: inputKey]) {
                [self.iErrorlLabel setHidden:YES];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"license"];
                
                BOOL adminPassEmty = [[self.iAdminPassword stringValue] isEqualToString:@""];
                BOOL adminPassCorrect = [[self.iAdminPassword stringValue] isEqualToString: [self.iAdminPasswordRepeate stringValue]];
                
                if (adminPassCorrect && !adminPassEmty) {
                    
                    UserModel *adminUser = [UserModel new];
                    adminUser.login = @"Admin";
                    adminUser.password = [self.iAdminPassword stringValue];
                    [adminUser saveUser];
                    
                    [self.mainVC showViewControllerWithName:@"LoginView"];
                    
                } else {
                    
                    if (adminPassEmty) {
                        [self.iErrorlLabel setHidden:NO];
                        [self.iErrorlLabel setStringValue:@"Password did not match"];
                    }
                    
                    if (adminPassEmty) {
                        [self.iErrorlLabel setHidden:NO];
                        [self.iErrorlLabel setStringValue:@"Admin pass empty"];
                    }
                    
                }
                
                
            } else {
                [self.iErrorlLabel setHidden:NO];
                [self.iErrorlLabel setStringValue:@"Wrong License Key"];
            }

            
        } else {
            
            [self.iErrorlLabel setHidden:NO];
            [self.iErrorlLabel setStringValue:@"No internet connection"];
        }
        
        
    }];
    
}


@end



