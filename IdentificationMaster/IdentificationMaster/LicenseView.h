//
//  LicenseView.h
//  IdentificationMaster
//
//  Created by Рома on 3/24/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"

@interface LicenseView : NSViewController

@property (strong) IBOutlet NSTextField *iLicenseInputField;

@property (strong) IBOutlet NSButton *iRegisterSoftwareButton;

@property (strong) IBOutlet NSTextField *iErrorlLabel;



typedef void (^DictionaryResultBlock)(NSDictionary *object, NSError *error);

- (void)checkInputLicenseOnServer:(NSString *)inpulLicense withBlock:(DictionaryResultBlock)block;

@property (strong) ViewController *mainVC;

@end
