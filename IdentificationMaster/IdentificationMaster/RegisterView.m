//
//  RegisterView.m
//  IdentificationMaster
//
//  Created by Рома on 3/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "RegisterView.h"

#import "NSString+JStringValidation.h"
#import "UserModel.h"

@interface RegisterView ()

@property (strong) IBOutlet NSTextField *iLoginTextField;

@property (strong) IBOutlet NSTextField *iPasswordTextField;

@property (strong) IBOutlet NSTextField *iRepeatPasswordTextField;

@property (strong) IBOutlet NSTextField *iErrorLabel;

@property (strong) IBOutlet NSButton *iRegisterButton;

@end

@implementation RegisterView


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}


- (IBAction)iRegisterButtonDidPushed:(id)sender {
    
    NSArray *allUsers = [UserModel allUsers];
    
    NSString *iLogin    = [self.iLoginTextField       stringValue];
    NSString *iPassword = [self.iPasswordTextField    stringValue];

    
    if ([self loginPasswordValidate] && [self passwordMatch]) {
        
        [self.iErrorLabel setHidden:YES];
        BOOL userWithThisLoginAlreadyExist = NO;
        
        for (NSDictionary *userInfo in allUsers) {
            
            UserModel *tempUser = [UserModel new];
            tempUser.login      = [userInfo objectForKey:@"login"];
            tempUser.password   = [userInfo objectForKey:@"password"];
            
            if ([iLogin isEqualToString: tempUser.login]) {
                userWithThisLoginAlreadyExist = YES;
            }
        }
        
        if (!userWithThisLoginAlreadyExist) {
            
            UserModel *newUser = [UserModel new];
            newUser.login = iLogin;
            newUser.password = iPassword;
            
            [newUser saveUser];
            [self.mainVC showViewControllerWithName:@"AdminView"];
            
        } else {
            [self.iErrorLabel setHidden:NO];
            [self.iErrorLabel setStringValue:@"User with this login already exist"];
        }
        
    } else {
        
        [self.iErrorLabel setHidden:NO];
    }

    
    
}


- (BOOL)loginPasswordValidate
{
    if ([[self.iLoginTextField stringValue] nameValidation]) {
        
        if ([[self.iPasswordTextField stringValue] passwordValidation]) {
            
            return YES;
        } else {
            
            [self.iErrorLabel setHidden:NO];
            [self.iErrorLabel setStringValue:@"Password incorrect"];
        }
        
    } else {
        
        [self.iErrorLabel setHidden:NO];
        [self.iErrorLabel setStringValue:@"Login incorrect"];
    }
    
    return NO;
}


- (BOOL)passwordMatch
{
    if ([[self.iPasswordTextField stringValue] isEqualToString:[self.iRepeatPasswordTextField stringValue]]) {
        return YES;
    }
    
    [self.iErrorLabel setHidden:NO];
    [self.iErrorLabel setStringValue:@"Password did not match"];
    return NO;
}


@end
