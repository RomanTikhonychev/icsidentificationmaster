//
//  ViewController.h
//  IdentificationMaster
//
//  Created by Рома on 3/17/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"
#import "UserModel.h"

@interface ViewController : NSViewController

@property (strong) UserModel *logedUser;

- (void)showViewControllerWithName:(NSString *)name;

@end

