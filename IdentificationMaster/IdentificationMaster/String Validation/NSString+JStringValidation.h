//
//  NSString+MMStringValidation.h
//  Test
//
//  Created by Рома on 2/3/16.
//  Copyright © 2016 RomanTikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JStringValidation)

- (BOOL)nameValidation;
- (BOOL)emailValidation;
- (BOOL)passwordValidation;
- (BOOL)phoneValidation;

@end
