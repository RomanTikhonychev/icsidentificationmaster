//
//  RegisterView.h
//  IdentificationMaster
//
//  Created by Рома on 3/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"

@interface RegisterView : NSViewController

@property (strong) ViewController *mainVC;

@end
