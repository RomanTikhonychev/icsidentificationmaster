//
//  UserModel.h
//  IdentificationMaster
//
//  Created by Рома on 3/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *userBlocked;
@property (nonatomic, strong) NSString *passRuleON;

- (void)saveUser;
+ (NSArray *)allUsers;
+ (NSString *)changePasswordForUserWithLogin:(NSString *)login withOldPass:(NSString *)oldPassword newPass:(NSString *)newPassword;
+ (NSString *)setBlockValueTo:(NSString *)blockValue forUserWithLogin:(NSString *)login;
+ (NSString *)setPassRuleValueTo:(NSString *)passRuleValue forUserWithLogin:(NSString *)login;


@end
