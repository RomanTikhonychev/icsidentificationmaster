//
//  AdminView.m
//  IdentificationMaster
//
//  Created by Рома on 3/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "AdminView.h"
#import "UserModel.h"

@interface AdminView ()

@end

@implementation AdminView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.iErrorMessageLabel setHidden:YES];

    [self setupUsersInfoTextView];
    // Do view setup here.
}

- (void)setupUsersInfoTextView
{
    NSArray *allUsers = [UserModel allUsers];
    NSString *infoString = @"";
    
    for (NSDictionary *userInfo in allUsers) {
        NSString *tempLogin     = [userInfo objectForKey:@"login"];
        NSString *tempPassword  = [userInfo objectForKey:@"password"];
        NSString *tempBlock     = [userInfo objectForKey:@"block"];
        NSString *tempPassRule  = [userInfo objectForKey:@"passRule"];
        
        
        NSString *apendString = [[NSString alloc] initWithFormat:@"*Login: %@ Pass: %@ \nBlocked %@ PassRule: %@\n",
                                 tempLogin, tempPassword, tempBlock, tempPassRule];
        
        infoString = [infoString stringByAppendingString:apendString];
        
        if (([tempPassRule boolValue]) && [tempLogin isEqualToString:tempPassword]) {
            infoString = [infoString stringByAppendingString: [[NSString alloc] initWithFormat:@"***NEED TO CHANGE PASS FOR %@\n",tempLogin]];
        }
    }
    
    [self.iUsersInfoTextView setStringValue:infoString];
}

- (IBAction)iChangeButtonDidPushed:(id)sender {
    
    NSString *changeUserLogin = [self.iChangePasswordForUserTextField stringValue];
    NSString *oldPassword     = [self.iOldPasswordTextField stringValue];
    NSString *newPassword     = [self.iNewPasswordTextField stringValue];
    NSString *newPasswordRepeat = [self.iRepeatPasswordTextField stringValue];
    
    NSString *resultString = @"Validation Error";
    
    if (YES) {
        
        resultString =  [UserModel changePasswordForUserWithLogin:changeUserLogin withOldPass:oldPassword newPass:newPassword];
    }
    
    [self setupUsersInfoTextView];
    [self.iErrorMessageLabel setHidden:NO];
    [self.iErrorMessageLabel setStringValue:resultString];
}

- (IBAction)iNewUserButtonDidPushed:(id)sender {
    [self.mainVC showViewControllerWithName:@"RegisterView"];
}


#pragma mark - rules Buttons

- (IBAction)blockUserButtonDidPushed:(id)sender {
    
    NSString *changeUserLogin = [self.iChangePasswordForUserTextField stringValue];
    NSString *resultString = [UserModel setBlockValueTo:@"YES" forUserWithLogin:changeUserLogin];
    [self setupUsersInfoTextView];
    [self.iErrorMessageLabel setHidden:NO];
    [self.iErrorMessageLabel setStringValue:resultString];
    
}

- (IBAction)unblockUserButtonDidPushed:(id)sender {
    NSString *changeUserLogin = [self.iChangePasswordForUserTextField stringValue];
    NSString *resultString = [UserModel setBlockValueTo:@"NO" forUserWithLogin:changeUserLogin];
    [self setupUsersInfoTextView];
    [self.iErrorMessageLabel setHidden:NO];
    [self.iErrorMessageLabel setStringValue:resultString];

}

- (IBAction)addPassRuleButtonDidPushed:(id)sender {
    NSString *changeUserLogin = [self.iChangePasswordForUserTextField stringValue];
    NSString *resultString = [UserModel setPassRuleValueTo:@"YES" forUserWithLogin:changeUserLogin];
    [self setupUsersInfoTextView];
    [self.iErrorMessageLabel setHidden:NO];
    [self.iErrorMessageLabel setStringValue:resultString];
}

- (IBAction)removePassRuleButtonDidPushed:(id)sender {
    NSString *changeUserLogin = [self.iChangePasswordForUserTextField stringValue];
    NSString *resultString = [UserModel setPassRuleValueTo:@"NO" forUserWithLogin:changeUserLogin];
    [self setupUsersInfoTextView];
    [self.iErrorMessageLabel setHidden:NO];
    [self.iErrorMessageLabel setStringValue:resultString];

}


@end
























