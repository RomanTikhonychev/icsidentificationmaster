//
//  AdminView.h
//  IdentificationMaster
//
//  Created by Рома on 3/25/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"
#import "UserModel.h"

@interface AdminView : NSViewController

@property (strong) IBOutlet NSTextField *iUserLoginLabel;

@property (strong) IBOutlet NSTextField *iChangePasswordForLabel;


@property (strong) IBOutlet NSTextField *iOldPasswordTextField;
@property (strong) IBOutlet NSTextField *iNewPasswordTextField;
@property (strong) IBOutlet NSTextField *iRepeatPasswordTextField;

@property (strong) IBOutlet NSButton *iChangeButton;

@property (strong) IBOutlet NSTextField *iUsersInfoTextView;

@property (strong) IBOutlet NSTextField *iChangePasswordForUserTextField;

@property (strong) IBOutlet NSTextField *iErrorMessageLabel;

//rules




@property (strong) ViewController *mainVC;

@property (strong) UserModel *adminUser;

@end
