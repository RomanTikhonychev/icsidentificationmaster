//
//  ViewController.m
//  IdentificationMaster
//
//  Created by Рома on 3/17/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "ViewController.h"

#import "LicenseView.h"
#import "LoginView.h"
#import "RegisterView.h"
#import "AdminView.h"
#import "UserView.h"

@interface ViewController ()

@property (strong) IBOutlet NSViewController *myViewController;
@property (strong) IBOutlet NSView           *myCustomView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    BOOL licenseDone = [[[NSUserDefaults standardUserDefaults] objectForKey:@"license"] boolValue];
    
    if (licenseDone) {
        
        [self showViewControllerWithName:@"LoginView"];
        
    } else {
        
        [self showViewControllerWithName:@"LicenseView"];
    }
    
}

- (void)showViewControllerWithName:(NSString *)name
{
    
    if ([name isEqualToString:@"LicenseView"]) {
        
        LicenseView *nextVC = [[LicenseView alloc] initWithNibName:@"LicenseView" bundle:nil];
        nextVC.mainVC = self;
        self.myViewController = nextVC;
        
        [[self.myCustomView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.myCustomView addSubview:self.myViewController.view];
        [[self.myViewController view] setFrame:[self.myCustomView bounds]];
        
    } else if ([name isEqualToString:@"LoginView"]) {
        
        LoginView *nextVC = [[LoginView alloc] initWithNibName:@"LoginView" bundle:nil];
        nextVC.mainVC = self;
        self.myViewController = nextVC;
        
        [[self.myCustomView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.myCustomView addSubview:self.myViewController.view];
        [[self.myViewController view] setFrame:[self.myCustomView bounds]];
        
    } else if ([name isEqualToString:@"RegisterView"]) {
        
        RegisterView *nextVC = [[RegisterView alloc] initWithNibName:@"RegisterView" bundle:nil];
        nextVC.mainVC = self;
        self.myViewController = nextVC;
        
        [[self.myCustomView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.myCustomView addSubview:self.myViewController.view];
        [[self.myViewController view] setFrame:[self.myCustomView bounds]];
        
    } else if ([name isEqualToString:@"AdminView"]) {
        
        AdminView *nextVC = [[AdminView alloc] initWithNibName:@"AdminView" bundle:nil];
        nextVC.mainVC = self;
        nextVC.adminUser = self.logedUser;
        self.myViewController = nextVC;
        
        [[self.myCustomView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.myCustomView addSubview:self.myViewController.view];
        [[self.myViewController view] setFrame:[self.myCustomView bounds]];
        
    } else if ([name isEqualToString:@"UserView"]) {
        
        UserView *nextVC = [[UserView alloc] initWithNibName:@"UserView" bundle:nil];
        nextVC.mainVC = self;
        nextVC.currentUser = self.logedUser;
        self.myViewController = nextVC;
        
        [[self.myCustomView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.myCustomView addSubview:self.myViewController.view];
        [[self.myViewController view] setFrame:[self.myCustomView bounds]];
        
    } else if ([name isEqualToString:@""]) {
       
        
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end
