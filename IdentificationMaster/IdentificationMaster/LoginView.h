//
//  LoginView.h
//  IdentificationMaster
//
//  Created by Рома on 3/24/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"

@interface LoginView : NSViewController

@property (strong) IBOutlet NSTextField *iLoginTextField;

@property (strong) IBOutlet NSTextField *iPasswordTextField;

@property (strong) IBOutlet NSButton *iLoginButton;

@property (strong) IBOutlet NSTextField *iErrorMessageLabel;

@property (strong) ViewController *mainVC;
@end
