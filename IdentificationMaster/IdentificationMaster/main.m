//
//  main.m
//  IdentificationMaster
//
//  Created by Рома on 3/17/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
