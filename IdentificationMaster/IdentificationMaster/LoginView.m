//
//  LoginView.m
//  IdentificationMaster
//
//  Created by Рома on 3/24/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "LoginView.h"
#import "UserModel.h"

@interface LoginView ()

@end

@implementation LoginView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)iLoginButtonDidPushed:(id)sender {
    
    [self.iErrorMessageLabel setHidden:YES];
    NSArray *allUsers = [UserModel allUsers];
    
    NSString *iLogin    = [self.iLoginTextField     stringValue];
    NSString *iPassword = [self.iPasswordTextField  stringValue];
    BOOL loginDone = NO;
    BOOL userBlocked = NO;
    BOOL userPassRuleON = NO;
    
    for (NSDictionary *userInfo in allUsers) {
        
        UserModel *indexUser    = [UserModel new];
        indexUser.login         = [userInfo objectForKey:@"login"];
        indexUser.password      = [userInfo objectForKey:@"password"];
        indexUser.userBlocked   = [userInfo objectForKey:@"block"];
        indexUser.passRuleON    = [userInfo objectForKey:@"passRule"];
        
            
            if ([indexUser.login isEqualToString:iLogin] && [indexUser.password isEqualToString:iPassword]) {
                
                if (![indexUser.userBlocked boolValue]) {
                
                    if (![indexUser.passRuleON boolValue]) {
                        
                        loginDone = YES;
                        if ([iLogin isEqualToString:@"Admin"]) {
                            
                            self.mainVC.logedUser = indexUser;
                            [self.mainVC showViewControllerWithName:@"AdminView"];
                            
                        } else {
                            
                            self.mainVC.logedUser = indexUser;
                            [self.mainVC showViewControllerWithName:@"UserView"];
                        }
                    } else {
                        
                        userPassRuleON = YES;
                        [self.iErrorMessageLabel setHidden:NO];
                        [self.iErrorMessageLabel setStringValue:@"Password invalid, ask Admin to solve this"];
                    }

                    
                } else {
                    
                    userBlocked = YES;
                    [self.iErrorMessageLabel setHidden:NO];
                    [self.iErrorMessageLabel setStringValue:@"User blocked by admin"];
                }
                
            }
        
    }
    
    
    if (!loginDone && !userBlocked && !userPassRuleON) {
        [self.iErrorMessageLabel setHidden:NO];
        [self.iErrorMessageLabel setStringValue:@"Login error"];
    } else if (!loginDone && userBlocked) {
        [self.iErrorMessageLabel setHidden:NO];
        [self.iErrorMessageLabel setStringValue:@"User blocked by admin"];
    } else if (!loginDone && !userBlocked && userPassRuleON) {
        [self.iErrorMessageLabel setHidden:NO];
        [self.iErrorMessageLabel setStringValue:@"Password invalid, ask Admin to solve this"];
    }
   
}


@end
