//
//  NSString+MMStringValidation.m
//  Test
//
//  Created by Рома on 2/3/16.
//  Copyright © 2016 RomanTikhonychev. All rights reserved.
//

#import "NSString+JStringValidation.h"

@implementation NSString (JStringValidation)


#pragma mark - JStringValidation


- (BOOL)passwordValidation
{
    
    if ([self length] < 8) {
        
        return NO;
    }
    
//    if ([self rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].location == NSNotFound) {
//        return NO;
//    }
    
//    if ([self rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound) {
//        return NO;
//    }
    
    return YES;
}


- (BOOL)emailValidation
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [predicate evaluateWithObject:self];
}

- (BOOL)nameValidation
{
    NSString *nameRegex =  @"[a-zA-z]+([ '-][a-zA-Z]+)*$"; //@"[A-Za-z.]+ [A-Za-z.]+ [A-Za-z.]";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [predicate evaluateWithObject:self];
}

- (BOOL)phoneValidation
{
    NSString *phoneRegex = @"\\+?[0-9]{6,15}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [predicate evaluateWithObject:self];
}


@end
